package ex2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * a Servlet that responsible to display the survey
 @author adibenezra
 */

@WebServlet(name = "DisplayPollServlet",  urlPatterns = "/DisplayPollServlet")
public class DisplayPollServlet extends HttpServlet {

    /**
     * the POST request
     * If the user doesn't vote, he comes here from the HandleCookieServlet and display the survey to the user.
     * DisplayPollServlet get the pollArray from the ContextServlet
     *
     @param request
     @param response
     @throws ServletException
     @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter toClient = response.getWriter();
        response.setContentType("text/html");
        request.getRequestDispatcher("home.html").include(request,response);

        ArrayList<String> arrayList = (ArrayList<String>) this.getServletContext().getAttribute("pollArray");

        toClient.println("<form action=\"SubmitServlet\" method=\"post\">\n" +
                            "</div>"  + arrayList.get(1) + "</div><br><br>" );

        for (int i=2; i < arrayList.size(); i++)
            toClient.println("  <input type=\"radio\" name=\"question\" value=" + (i-2) + ">\n" +
                    "  <label for=" + (i-2) + ">" + arrayList.get(i) + "</label><br>");

        toClient.println("<br><input type=\"submit\" value=\"Submit\">\n </div> </form>");

        toClient.close();
    }
}
