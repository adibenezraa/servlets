package ex2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * a Servlet that start the program and do "include" to the ReadFileServlet
 @author adibenezra
 */

@WebServlet(name = "StartServlet", urlPatterns = "/StartServlet")
public class StartServlet extends HttpServlet {

    /**
     * the GET request
     @param request
     @param response
     @throws ServletException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
        try {
            //"include" to ReadFileServlet
            request.getRequestDispatcher("ReadFileServlet").include(request, response);
        }
        catch (IOException e){
            System.out.println("bad input");
            System.exit(0);
        }

    }
}
