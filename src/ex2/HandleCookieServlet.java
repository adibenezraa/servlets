package ex2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * a servlet that handle the cookies
 @author adibenezra
 */

@WebServlet(name = "HandleCookieServlet", urlPatterns = "/HandleCookieServlet")

    public class HandleCookieServlet extends HttpServlet {
    private String  user;

    /**
     * the POST request
     * When the user tries to vote for the poll he comes here to check
     * if he has already voted in the past by using the cookie that comes in the request.
     * if not, the Servlet does "include" to DisplayPollServlet
     @param request
     @param response
     @throws ServletException
     @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Cookie[] cookies = request.getCookies();
        for (int i = 0; cookies != null && i < cookies.length; i++) {
            if (cookies[i].getName().equals("username")) {
                user = cookies[i].getValue();
                response.setContentType("text/html");
                response.getWriter().println("</h1> Welcome Back!<br>" +
                        "We have your vote. You will not be able to vote again</h1><br><br>\n" +
                        "<form action=\"DisplayVotersServlet\" method=\"get\">\n" +
                        "<button id=\"submit\" type=\"submit\">Back to HomePage</button></form>\n");
                response.getWriter().close();
            }
        }
        response.addCookie(new Cookie("username", user));
        request.getRequestDispatcher("DisplayPollServlet").include(request, response);
    }
}