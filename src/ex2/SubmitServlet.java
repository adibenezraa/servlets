package ex2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * a Servlet that responsible the submit action.
 @author adibenezra
 */

@WebServlet(name = "SubmitServlet", urlPatterns = "/SubmitServlet")
public class SubmitServlet extends HttpServlet {

    /**
     * the POST request
     * When the user input the response to the question, he click the submit and
     * then the Handel Cookie Servlet send hem to this Servlet.
     * The Submit Servlet update the voters global array using the ContextServlet according to the request,
     * then, the servlet do "include" to DisplayPollServlet
     @param request
     @param response
     @throws ServletException
     @throws IOException
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter toClient = response.getWriter();
        response.setContentType("text/html");
        request.getRequestDispatcher("home.html").include(request,response);

        String question = request.getParameter("question");

        if (question != null) {

            ArrayList<Integer> votersArray = (ArrayList<Integer>) this.getServletContext().getAttribute("votersArray");

            votersArray.set(Integer.parseInt(question), votersArray.get(Integer.parseInt(question)) + 1);
            this.getServletContext().setAttribute("votersArray", votersArray);
            toClient.println("</h1> Thank You! :) </h1><br><br>\n" +
                            "<form action=\"DisplayVotersServlet\" method=\"get\">\n" +
                            "<button id=\"submit\" type=\"submit\">Back to HomePage</button></form>\n");
            toClient.close();
        }

        else
            request.getRequestDispatcher("DisplayPollServlet").include(request, response);

    }


}
