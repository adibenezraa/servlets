package ex2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;

/**
 * a Servlet that responsible to display the most up-to-date poll votes
 @author adibenezra
 */

@WebServlet(name = "DisplayVotersServlet", urlPatterns = "/DisplayVotersServlet")
public class DisplayVotersServlet extends HttpServlet {

    /**
     * Reading init params for the servlet.
     * Init the votersArray to 0 and set to the ContextServlet.
     * Set votersArray size using the global pollArray that created by the readFileServlet.
     @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();

        ArrayList<String> arrayList = (ArrayList<String>) this.getServletContext().getAttribute("pollArray");
        ArrayList<Integer> votersArray = new ArrayList<Integer>(Collections.nCopies(arrayList.size() - 2, 0));
        this.getServletContext().setAttribute("votersArray", votersArray);
    }

    /**
     * the GET request
     * when the ReadFile
     * When ReadFileServlet finishes reading the file it does "include" to that Servlet.
     * DisplayVotersServlet Displays the most up-to-date poll votes
     @param request
     @param response
     @throws ServletException
     @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter toClient = response.getWriter();
        response.setContentType("text/html");
        request.getRequestDispatcher("home.html").include(request,response);

        ArrayList<String> pollArray = (ArrayList<String>) this.getServletContext().getAttribute("pollArray");
        ArrayList<Integer> votersArray = (ArrayList<Integer>) this.getServletContext().getAttribute("votersArray");

        toClient.println("</h1> Hey!</h1><br><br>\n" +
                         "</h2> Please take a minute to answer a short question.</h2><br>\n" +
                         "</h3> Question: "+ pollArray.get(1) + "</h3><br><br>\n" +
                         "<form action=\"HandleCookieServlet\" method=\"post\">\n" +
                         "<button id=\"submit\" type=\"submit\">Vote Here!</button></form>\n" +
                         "<table style=\"width:50%\">\n" +
                         "  <tr>\n" +
                         "      <th>Answer</th>\n" +
                         "      <th>Total Votes</th> \n" +
                         "  </tr><br>\n");

        for (int i = 2; i < pollArray.size(); i++)
            toClient.println(" <tr>\n" +
                    "    <td>" + pollArray.get(i) + "</td>\n" +
                    "    <td>" + votersArray.get(i - 2)+ "</td>\n" +
                    "  </tr>\n"
                    );
           toClient.println("</table>");
    }
}
