package ex2;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import javax.servlet.annotation.WebInitParam;

/**
 * a Servlet that responsible to read the input file and set to a global array using ContextServlet
 @author adibenezra
 */

@WebServlet(name = "ReadFileServlet", urlPatterns = "/ReadFileServlet",
        initParams =  { @WebInitParam(name = "fileName", value = "poll.txt")})
public class ReadFileServlet extends HttpServlet {

    private String file;

    /**
     * reading init params for the servlet
     * read once the fileName from the "init param" and set to this.file
     @param config
     @throws ServletException
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        this.file = config.getInitParameter("fileName");
    }


    /**
     * the GET request
     * when  StartServlet do the "include" to ReadFileServlet the doGet func is execute
     * this servlet get the input file using the Context Servlet by the getResource method
     * Read the question and the response from the file and insert all of these to a global array by the Context Servlet
     * then, do "include" to DisplayVotersServlet
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        URL url = this.getServletContext().getResource(this.file);
        BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

        ArrayList<String> array = new ArrayList<>();
        for (String s = new String(); s != null; s = reader.readLine())
            array.add(s);

        this.getServletContext().setAttribute("pollArray", array);

        reader.close();
        request.getRequestDispatcher("DisplayVotersServlet").include(request, response);
    }

}
